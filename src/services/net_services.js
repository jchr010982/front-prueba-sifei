import { api } from "boot/axios.js";
import { api_sec } from "boot/axios.js";
import { useAuthStore } from "../stores/auth.js";

const authStore = useAuthStore();
const token = authStore.token;
console.log(token);
if (token) {
  api.defaults.headers.common["Authorization"] = "Bearer " + token;
}

export async function login(usuario, clave) {
  return await api
    .post("login/", {
      username: usuario,
      password: clave,
    })
    .then((response) => {
      console.log(response);
      api.defaults.headers.common["Authorization"] =
        "Bearer " + response.data.token;
      authStore.setToken(response.data.token);
      return response.data;
    })
    .catch((error) => {
      return error;
    });
}

export function obtener_productos() {
  return api.get("productos/");
}

export function guardar_carrito(carrito) {
  return api.post("compras/", carrito);
}
