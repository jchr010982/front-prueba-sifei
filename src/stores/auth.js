import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    token: localStorage.getItem("token") || null,
    name: localStorage.getItem("name") || null,
    user: localStorage.getItem("user") || null,
    id: localStorage.getItem("id") || null,
  }),
  getters: {
    getToken: (state) => state.token,
    getName: (state) => state.name,
    getUser: (state) => state.user,
    getId: (state) => state.id,
  },
  actions: {
    setToken(token) {
      this.token = token;
      localStorage.setItem("token", token); // Guardar el token en localStorage
    },
    clearToken() {
      this.token = null;
      localStorage.removeItem("token"); // Eliminar el token de localStorage
    },
    setName(name) {
      this.name = name;
      localStorage.setItem("name", name); // Guardar el token en localStorage
    },
    setUser(user) {
      this.user = user;
      localStorage.setItem("user", user); // Guardar el token en localStorage
    },
    setId(id) {
      this.id = id;
      localStorage.setItem("id", id); // Guardar el token en localStorage
    },
  },
});
