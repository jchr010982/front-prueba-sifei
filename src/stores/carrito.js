import { defineStore } from "pinia";

export const carritoStore = defineStore("carrito", {
  state: () => ({
    carrito: localStorage.getItem("carrito") || [],
  }),
  getters: {
    getCarrito: (state) => state.carrito,
  },
  actions: {
    agregaProducto(producto) {
      console.log(typeof this.carrito);
      this.carrito = this.carrito.filter((x) => x.clave != producto.clave);
      this.carrito.push(producto);
      localStorage.setItem("carrito", this.carrito); // Guardar el carrito en localStorage
    },
    eliminaProducto(producto) {
      this.carrito = this.carrito.filter((x) => x.clave != producto.clave);
      localStorage.setItem("carrito", this.carrito); // Guardar el carrito en localStorage
    },
    limpiaCarrito() {
      this.carrito = [];
      localStorage.setItem("carrito", []); // Guardar el carrito en localStorage
    },
  },
});
